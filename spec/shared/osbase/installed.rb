shared_examples 'osbase::installed' do

  context 'osbase on Redhat', :if => os[:family] == 'redhat' do
    context 'osbase::packages::install on Redhat' do
      describe 'vim-enhanced' do
        it { expect(package('vim-enhanced')).to be_installed }
      end
    end
  end

  context 'osbase on Debian', :if => os[:family] == 'debian' do
    context 'osbase::packages::install on Debian' do
      describe 'vim' do
        it { expect(package('vim')).to be_installed }
      end
    end
  end

  context 'osbase on all OS' do
    context 'osbase::packages::install be_installed' do
      describe package('curl') do
        it { is_expected.to be_installed }
      end
      describe package('wget') do
        it { is_expected.to be_installed }
      end
      describe 'ftp, sysstat, htop, iotop, coreutils, less, net-tools, tcpdump, lsof' do
        it { expect(package('ftp')).to be_installed }
        it { expect(package('sysstat')).to be_installed }
        it { expect(package('htop')).to be_installed }
        it { expect(package('iotop')).to be_installed }
        it { expect(package('coreutils')).to be_installed }
        it { expect(package('less')).to be_installed }
        it { expect(package('net-tools')).to be_installed }
        it { expect(package('tcpdump')).to be_installed }
        it { expect(package('lsof')).to be_installed }
      end
    end

  end
end
